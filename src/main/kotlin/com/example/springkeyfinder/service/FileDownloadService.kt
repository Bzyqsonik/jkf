package com.example.springkeyfinder.service

import org.springframework.stereotype.Service
import org.springframework.web.reactive.function.client.WebClient
import org.springframework.web.reactive.function.client.bodyToMono
import reactor.core.publisher.Mono
import java.time.LocalDateTime


@Service
class FileDownloadService {

    fun download(url: String): Mono<FileDownloadResponse> {
        println("${LocalDateTime.now()} Started downloading file $url")
        return WebClient.builder()
            .baseUrl("http://localhost:3000")
            .build()
            .get()
            .uri("/download?url=$url")
            .retrieve()
            .bodyToMono<FileDownloadResponse>()
            .doOnNext { println("${LocalDateTime.now()} Downloaded file ${it.id}") }
    }
}

data class FileDownloadResponse(
    val id: String,
    val title: String
)