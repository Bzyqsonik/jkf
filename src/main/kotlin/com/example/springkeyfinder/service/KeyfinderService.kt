package com.example.springkeyfinder.service

import com.example.springkeyfinder.repository.KeyResult
import org.springframework.stereotype.Service
import reactor.core.publisher.Mono
import reactor.core.scheduler.Schedulers
import java.math.RoundingMode
import java.nio.file.Files
import java.nio.file.Paths

private val URL_WITH_PARAMS_REGEX = Regex(".*v=(?<ytId>.*)&.*")
private val URL_REGEX = Regex(".*v=(?<ytId>.*)")
private val YOUTU_BE_REGEX = Regex("youtu.be/(?<ytId>.*)")

@Service
class KeyfinderService(
    private val fileDownloadService: FileDownloadService,
    private val keyDecodeService: KeyDecodeService,
    private val saveKeyFinderResultService: SaveKeyFinderResultService,
    private val readKeyFinderResultService: ReadKeyFinderResultService
) {

    fun find(url: String) =
        readKeyFinderResultService.readByYtId(url.extractYtId())
            .map { it.id }
            .switchIfEmpty(downloadAndDecode(url.extractYtId()))

    private fun downloadAndDecode(ytId: String) =
        fileDownloadService.download(ytId)
            .flatMap { result ->
                keyDecodeService.decode(result.id)
                    .doOnNext {
                        saveKeyFinderResultService.save(
                            KeyResult(
                                id = result.id,
                                ytId = ytId,
                                key = it.key,
                                title = result.title,
                                bpm = it.bpm?.toBigDecimal()?.setScale(2, RoundingMode.HALF_EVEN)
                            )
                        )
                    }
                    .doOnNext {
                        Mono.fromRunnable<Void> {
                            Files.deleteIfExists(Paths.get("$it.webm"))
                        }
                            .subscribeOn(Schedulers.boundedElastic())
                            .subscribe()
                    }
                    .map { result.id }
            }

    private fun String.extractYtId() =
        URL_WITH_PARAMS_REGEX.extract(this)
            ?: URL_REGEX.extract(this)
            ?: YOUTU_BE_REGEX.extract(this)
            ?: throw IllegalStateException("Oops")

    private fun Regex.extract(string: String) = find(string)?.let {
        it.groups["ytId"]?.value
    }
}