package com.example.springkeyfinder.service

import com.example.springkeyfinder.repository.KeyResultRepository
import org.springframework.stereotype.Service

@Service
class ReadKeyFinderResultService(private val keyResultRepository: KeyResultRepository) {

    fun readById(id: String) = keyResultRepository.findById(id)

    fun readByYtId(id: String) = keyResultRepository.findFirstByYtId(id)
}