package com.example.springkeyfinder.service

import com.example.javakeychain.JavaKeychain
import com.example.javakeychain.TrackAnalyzer
import org.springframework.stereotype.Service
import reactor.core.publisher.Mono
import reactor.core.publisher.Mono.fromSupplier
import reactor.core.scheduler.Schedulers

private val ELASTIC = Schedulers.boundedElastic()

@Service
class KeyDecodeService(private val javaKeychain: JavaKeychain) {

    fun decode(fileName: String): Mono<TrackAnalyzer.Result> = Mono.defer {
        fromSupplier {
            println("Decoding $fileName")
            javaKeychain.findKey("$fileName.webm")
        }
    }.subscribeOn(ELASTIC)
}