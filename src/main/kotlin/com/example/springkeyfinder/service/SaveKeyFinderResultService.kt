package com.example.springkeyfinder.service

import com.example.springkeyfinder.repository.KeyResult
import com.example.springkeyfinder.repository.KeyResultRepository
import org.springframework.stereotype.Service
import reactor.core.scheduler.Schedulers

@Service
class SaveKeyFinderResultService(private val keyResultRepository: KeyResultRepository) {

    fun save(result: KeyResult) {
        keyResultRepository.save(result)
            .subscribeOn(Schedulers.boundedElastic())
            .subscribe()
    }
}