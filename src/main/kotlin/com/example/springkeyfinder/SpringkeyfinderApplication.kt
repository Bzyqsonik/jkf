package com.example.springkeyfinder

import com.example.javakeychain.JavaKeychain
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

@SpringBootApplication
class SpringkeyfinderApplication

fun main(args: Array<String>) {
    runApplication<SpringkeyfinderApplication>(*args)
}

@Configuration
class SpringkeyfinderConfiguration {

    @Bean
    fun javaKeychain() = JavaKeychain()
}

