package com.example.springkeyfinder.repository

import org.springframework.data.annotation.Id
import org.springframework.data.mongodb.core.mapping.Document
import org.springframework.data.mongodb.repository.ReactiveMongoRepository
import reactor.core.publisher.Mono
import java.math.BigDecimal

interface KeyResultRepository : ReactiveMongoRepository<KeyResult, String> {

    fun findFirstByYtId(ytId: String): Mono<KeyResult>
}

@Document
data class KeyResult(
    @Id
    val id: String,
    val ytId: String,
    val key: String,
    val title: String,
    val bpm: BigDecimal? = null
)