package com.example.springkeyfinder.controller

import com.example.springkeyfinder.repository.KeyResult
import com.example.springkeyfinder.service.KeyfinderService
import com.example.springkeyfinder.service.ReadKeyFinderResultService
import org.springframework.http.MediaType
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.CrossOrigin
import org.springframework.web.bind.annotation.ExceptionHandler
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestMethod
import org.springframework.web.bind.annotation.RestController
import org.springframework.web.server.UnsupportedMediaTypeStatusException
import java.math.BigDecimal


@RestController
@RequestMapping("/api/keyfinder/results")
class KeyfinderController(
    private val keyfinderService: KeyfinderService,
    private val readKeyFinderResultService: ReadKeyFinderResultService
) {

    @PostMapping(produces = [MediaType.APPLICATION_JSON_VALUE])
    @CrossOrigin(origins = ["https://youtubekeyfinder.com"])
    fun decode(@RequestBody request: DecodeRequest) =
        keyfinderService.find(request.url)
            .map(::DecodeResponse)

    @GetMapping("/{id}")
    @CrossOrigin(origins = ["https://youtubekeyfinder.com"])
    fun getResult(@PathVariable id: String) =
        readKeyFinderResultService.readById(id)
            .map{ it.toResponse()}

    @RequestMapping(method = [RequestMethod.OPTIONS])
    fun preflight() = ResponseEntity.ok().header("Access-Control-Allow-Origin", "https://youtubekeyfinder.com").build<Any>()
}

private fun KeyResult.toResponse() = KeyResponse(
    key = key,
    title = title,
    bpm = bpm
)

data class DecodeRequest(
    val url: String
)

data class DecodeResponse(
    val result: String
)

data class KeyResponse(
    val key: String,
    val title: String,
    val bpm: BigDecimal? = null
)