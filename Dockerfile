FROM azul/zulu-openjdk:16

RUN apt-get update
RUN apt-get upgrade -y
RUN apt-get install -y curl
RUN curl -sL https://deb.nodesource.com/setup_14.x -o setup_14.sh
RUN bash ./setup_14.sh
RUN apt-get install nodejs -y
RUN apt-get install ffmpeg -y

COPY build/libs/springkeyfinder-0.0.1-SNAPSHOT.jar app.jar
COPY app.js .
COPY node_modules node_modules
COPY package-lock.json .
COPY package.json .

ENV app_name "JKeychain"

EXPOSE 3000
EXPOSE 8080
RUN npm install
RUN npm start &
ENTRYPOINT ["java", "-jar", "-Dserver.port=$PORT", "app.jar"]