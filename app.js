const express = require('express')
const {v4: uuidv4} = require('uuid');
const fs = require("fs");
const ytdl = require('ytdl-core');
const cors = require('cors');

const app = express()
app.use(cors());

const port = process.env.PORT || 3000

app.get('/', (req, res) => {
    res.sendStatus(404)
})

app.get('/download', (req, res) => {
    const id = uuidv4()
    const fileWriteStream = fs.createWriteStream(`${id}.webm`, {highWaterMark: 65556});
    var length = 0;
    var sent = false

    const stream = ytdl(req.query.url, {
        highWaterMark: 65556,
        dlChunkSize: 65556,
        quality: 'lowest',
        filter: "audio"
    })
    stream
        .on("data", function (data) {
            length += data.length;
            if (!sent && length > 250000) {
                sent = true
                stream.unpipe(fileWriteStream)
                stream.pause()
                stream.emit("finish")
            }
        })
        .on("finish", function () {
            ytdl.getInfo(req.query.url).then(value =>
                res.send({
                    id: id,
                    title: value.videoDetails.title
                }))
        })
        .pipe(fileWriteStream)

});

app.listen(port, () => {
    console.log(`Example app listening at http://localhost:${port}`)
})
